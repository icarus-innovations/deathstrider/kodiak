### Kodiak
---
This is an addon for Deathstrider that adds a 10 round .50 BMG Anti-Material Rifle. It's an M82 but not.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- Accensus, coding and gauntlet sprites
- Sprite assets used from Sonik.O.Fan (TSF), PillowBlaster, Mor'ladim (probably), IAmCarrotMaster and probably a few others I'm forgetting.
- Sounds from [GameBanana](https://gamebanana.com/mods/211053)

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.